# FEDERATED ONCOMPARE

## DOCKER image
The docker image ``ddecap/oncompare`` runs flwr with the required libraries present and the runscripts for Oncompare.
### Build image
To build the image, run this command in the ``flwr`` directory where the ``Dockerfile`` is:
```bash
# create flwr image
cd flwr/src
docker build --no-cache -t ddecap/flwr-extended . # can also be pulled from docker hub: docker pull ddecap/flwr-extended
# create oncompare image
cd .. # back to flwr/ dir outside of src
docker build --no-cache -t ddecap/oncompare .
```

### swarm setup
A swarm needs to be configured so all docker daemons (on different nodes) can communicate:
```bash
# run on a single node
docker swarm init # will be swarm manager
# on the other nodes join the swarm, but running the command provided, similar to:
docker swarm join --token SWMTKN-<unique-id> <ip-addr>:2377
```

### attachable overlay network
setup an overlay network between nodes, where a regular docker instance can also connect to:
```bash
# run this on a single node, needs to be run on the swarm manager
docker network create -d overlay --attachable my-attachable-overlay
```

### run Federated OnCompare
start the oncompare server and clients, the server needs to be run before starting the clients:
```bash
# start the client on a single node (i.e. on the swarm manager):
CLIENTS=2
MAX_ROUNDS=50
docker run --network=my-attachable-overlay ddecap/oncompare -s $CLIENTS $MAX_ROUNDS
# this will print out the hostname, which is the server for the clients to connect to:
server hostname is cf073c44b02b [ip-addrs: 10.0.1.4 172.18.0.3 ]

# on all clients (equal to the number in $CLIENTS):
# the input files $FILE should be present in your current working directory, or be in a subdirectory
FILE=data_hospital1.csv # per hospital/client
SERVER=cf073c44b02b  # the hostname the server provided
docker run -v $(pwd):/opt/oncompare/data --network=my-attachable-overlay ddecap/oncompare -c $FILE $SERVER

```

## Nix environment
A nix shell can be loaded, where python 3.10 and all required packages are available. Inside this shell you can simply run server.py and client.py.
### server
```bash
#start the environment:
cd flwr/src/
nix-shell flwr.nix
# this will start a new environment where python3, flwr and all dependencies are available
python ../server.py 2 50 # start server for 2 clients and max 50 rounds for log reg
```

### clients
For all clients, the environment also needs to be loaded:
```bash
#start the environment:
cd flwr/src/
nix-shell flwr.nix
# start clients, you will also have t
python  ../client.py $DATAFILE $SERVER_HOSTNAME
```

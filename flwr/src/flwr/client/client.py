# Copyright 2020 Adap GmbH. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Flower client (abstract base class)."""


from abc import ABC, abstractmethod

from flwr.common.logger import log
from logging import DEBUG, INFO
from flwr.common import (
    EvaluateIns,
    EvaluateRes,
    FitIns,
    FitRes,
    ParametersRes,
    PropertiesIns,
    PropertiesRes,
    AnalyticsIns,
    AnalyticsRes,
    EncryptedNumber,
)
from flwr.server.strategy.strategy_FA_wrapper import AggregationFunctionMap
import phe.paillier as paillier # homomorphic encryption

class Client(ABC):
    """Abstract base class for Flower clients."""

    def __init__(self):
        self.pub_key = None
        self.priv_key = None


    def get_properties(self, ins: PropertiesIns) -> PropertiesRes:
        """Return set of client's properties.

        Parameters
        ----------
        ins : PropertiesIns
            The get properties instructions received from the server containing
            a dictionary of configuration values used to configure.

        Returns
        -------
        PropertiesRes
            Client's properties.
        """

    @abstractmethod
    def get_parameters(self) -> ParametersRes:
        """Return the current local model parameters.

        Returns
        -------
        ParametersRes
            The current local model parameters.
        """

    @abstractmethod
    def fit(self, ins: FitIns) -> FitRes:
        """Refine the provided weights using the locally held dataset.

        Parameters
        ----------
        ins : FitIns
            The training instructions containing (global) model parameters
            received from the server and a dictionary of configuration values
            used to customize the local training process.

        Returns
        -------
        FitRes
            The training result containing updated parameters and other details
            such as the number of local training examples used for training.
        """

    @abstractmethod
    def evaluate(self, ins: EvaluateIns) -> EvaluateRes:
        """Evaluate the provided weights using the locally held dataset.

        Parameters
        ----------
        ins : EvaluateIns
            The evaluation instructions containing (global) model parameters
            received from the server and a dictionary of configuration values
            used to customize the local evaluation process.

        Returns
        -------
        EvaluateRes
            The evaluation result containing the loss on the local dataset and
            other details such as the number of local data examples used for
            evaluation.
        """

    def query_analytics_encryption_wrapper(self, ins: AnalyticsIns) -> AnalyticsRes:
        """Wrapper to enable encryption where needed for analytics data. """
        if(ins.encryption):
            if ins.query == "decrypt":
                log(DEBUG, "decrypting result")
                decrypted = []
                for num in ins.encrypted_number:
                    decrypted.append(self.priv_key.decrypt(num.value))
                # log(DEBUG, "result: %s", decrypted)
                return AnalyticsRes(num_examples=ins.num_examples, result=decrypted, encrypted_number=[])
            else:
                res = self.query_analytics(ins)
                log(DEBUG, "encrypting result...")
                if len(ins.encrypted_number) == 0:
                    if self.pub_key is None:
                        # setup homomorphic encryption keys:
                        log(DEBUG, "creating encryption keys")
                        self.pub_key, self.priv_key = paillier.generate_paillier_keypair()
                    # create first encrypted number
                    encrypted_number = []
                    for r in res.result:
                        encrypted_number.append(EncryptedNumber(self.pub_key.encrypt(r)))
                    return AnalyticsRes(num_examples=res.num_examples, result=[], encrypted_number=encrypted_number)
                elif len(ins.encrypted_number) == len(res.result):
                    # add result to encrypted_number
                    return AggregationFunctionMap[ins.aggregate_function]([AnalyticsRes(ins.num_examples, result=[], encrypted_number=ins.encrypted_number), res])
                else:
                    raise Exception("number of encrypted numbers in result must be the same as provided: " + str(len(ins.data)) + " != " + str(len(res.result)))
        else:
            res = self.query_analytics(ins)
            return res

    # default is empty funcution so we can check if it is implemented or not
    def query_analytics(self, ins: AnalyticsIns) -> AnalyticsRes:
        """Returns the query for analytics data.

        Parameters
        ----------
        ins : AnalyticsIns
            Contains a query string to identify the requested federated analytics
            and a config parameter. The config contains relevant variables to
            calculate the query.
            WARNING: Do not use 'encrypted_number' and 'encryption' unless you know
            how this works.

        Returns
        -------
        AnalyticsRes
            Contains the number of samples used in the query for federated analytics
            (num_examples) and the results itself. The result is a list Scalars.
            WARNING: The result should only contain a single value of homomorphic encryption
            is requested.
            WARNING: Do not use 'encrypted_number' unless you know how this works.
        """



def has_get_properties(client: Client) -> bool:
    """Check if Client implements get_properties."""
    return type(client).get_properties != Client.get_properties

def has_federated_analytics(client: Client) -> bool:
    """Check if Client implements query_analytics."""
    return type(client).query_analytics != Client.query_analytics

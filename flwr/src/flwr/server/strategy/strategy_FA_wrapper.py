

import concurrent.futures
from abc import ABC, abstractmethod
from typing import Callable, Dict, List, Optional, Tuple

import numpy as np
from flwr.common.logger import log
from logging import DEBUG, INFO
from flwr.common import EvaluateIns, EvaluateRes, AnalyticsIns, AnalyticsRes, Parameters, Scalar, Config, Metrics, FitIns, FitRes, EncryptedNumber
from flwr.server.client_manager import ClientManager
from flwr.server.client_proxy import ClientProxy
from flwr.server.strategy import Strategy
from flwr.proto.transport_pb2 import AggregateFunction
import phe.paillier as paillier # homomorphic encryption

AggregationFn = Callable[[AnalyticsRes], AnalyticsRes]


def sum_aggregate_fn(data: List[AnalyticsRes]) -> AnalyticsRes:
    count = 0
    return_data = [0]
    encrypted = False
    for d in data:
        count += d.num_examples
        for i in range(0, len(d.result)):
            return_data[0] += d.result[i]
        if len(d.encrypted_number) > 0:
            encrypted = True;
        for i in range(0, len(d.encrypted_number)):
            return_data[0] = d.encrypted_number[i].value + return_data[0]
    # log(INFO, "aggregated data: %s", summed_metrics)
    if encrypted:
        return AnalyticsRes(count, [], [EncryptedNumber(e) for e in return_data])
    else:
        return AnalyticsRes(count, return_data, [])

def concatenate_aggregate_fn(data: List[AnalyticsRes]) -> AnalyticsRes:
    count = 0
    return_data = []
    encrypted = False
    pub_key = None
    for d in data:
        # log(DEBUG, "data -> %s", d)
        count += d.num_examples
        for i in range(0, len(d.result)):
            return_data.append(d.result[i])
        if len(d.encrypted_number) > 0:
            encrypted = True;
        if len(d.encrypted_number) > 0 and pub_key == None:
            public_key=d.encrypted_number[0].value.public_key;
        for i in range(0, len(d.encrypted_number)): # here we need to encrypt all other data with the public key
            return_data.append(d.encrypted_number[i])
    # log(INFO, "aggregated data: %s", concat_metrics)
    if encrypted:
        return AnalyticsRes(count, [], [d if isinstance(d, EncryptedNumber) else EncryptedNumber(public_key.encrypt(d)) for d in return_data])
    else:
        return AnalyticsRes(count, return_data, [])

def column_sum_aggregate_fn(data: List[AnalyticsRes]) -> AnalyticsRes:
    count = 0
    return_data = [0] * (len(data[0].result) | len(data[0].encrypted_number))
    print(return_data)
    encrypted = False
    # decrypt data!
    for d in data:
        count += d.num_examples
        if len(d.result) == 0 and len(d.encrypted_number) == len(return_data):
            encrypted = True;
            for i in range(0, len(d.encrypted_number)):
                return_data[i] = d.encrypted_number[i].value + return_data[0]
        elif len(d.result) == len(return_data) and len(d.encrypted_number) == 0:
            for i in range(0, len(d.result)):
                return_data[i] += d.result[i]
        else:
            raise Exception("column_sum must have same number of columns in all results or encrypted numbers")
    if encrypted:
        return AnalyticsRes(count, [], [EncryptedNumber(e) for e in return_data])
    else:
        return AnalyticsRes(count, return_data, [])

AggregationFunctionMap = {  # add to AggregateFunction if needed (see transport.proto and rebuild)
    AggregateFunction.SUM: sum_aggregate_fn,
    AggregateFunction.CONCATENATE: concatenate_aggregate_fn,
    AggregateFunction.COLUMN_SUM: column_sum_aggregate_fn,
}

def get_aggregate_fn_enum_val(val):
    for key, value in AggregationFunctionMap.items():
         if val == value:
             return key

    raise Exception("Aggregate function " + str(val) + " not yet implemented")

class StrategyWithFederatedAnalyticsWrapper(Strategy):
    """Abstract base class for server strategy implementations."""

    def __init__(self, strategy: Strategy) -> None:
        self.strategy = strategy
        self.min_available_clients = strategy.min_available_clients
        # TODO: is this still needed?
        self.max_workers: Optional[int] = None
        self.timeout: Optional[float] = None


    # overwrite function with given strategy
    def initialize_parameters(
        self, client_manager: ClientManager
    ) -> Optional[Parameters]:
        return self.strategy.initialize_parameters(client_manager)

    def configure_fit(
        self, rnd: int, parameters: Parameters, client_manager: ClientManager
    ) -> List[Tuple[ClientProxy, FitIns]]:
        return self.strategy.configure_fit(rnd, parameters, client_manager)

    def aggregate_fit(
        self,
        rnd: int,
        results: List[Tuple[ClientProxy, FitRes]],
        failures: List[BaseException],
    ) -> Tuple[Optional[Parameters], Dict[str, Scalar]]:
        return self.strategy.aggregate_fit(rnd, results, failures)

    def configure_evaluate(
        self, rnd: int, parameters: Parameters, client_manager: ClientManager
    ) -> List[Tuple[ClientProxy, EvaluateIns]]:
        return self.strategy.configure_evaluate(rnd, parameters, client_manager)

    def aggregate_evaluate(
        self,
        rnd: int,
        results: List[Tuple[ClientProxy, EvaluateRes]],
        failures: List[BaseException],
    ) -> Tuple[Optional[float], Dict[str, Scalar]]:
        return self.strategy.aggregate_evaluate(rnd, results, failures)

    def evaluate(
        self, parameters: Parameters
    ) -> Optional[Tuple[float, Dict[str, Scalar]]]:
        return self.strategy.evaluate(parameters)


    # new functions
    @abstractmethod
    def run_federated_analytics_pre_fit(
       self, client_manager: ClientManager, timeout: Optional[float]
    ) -> None:
       """Get analytics from all clients.

       Arguments:
           client_manager: ClientManager. The client manager containing all currently connected clients.
           timeout: Optional[float]. timeout for client reply.

       Returns:
          Nothing.

      Use this function to run federated analytics:
      federated_analytics(
          self, query: str, config: Config, client_manager: ClientManager, timeout: Optional[float],
          aggregate_fn: Optional[AggregationFn],
          use_homomorphic_encryption: Optional[bool] = False,
      ) -> Tuple[int, Dict[str, Scalar]]
       """

    @abstractmethod
    def run_federated_analytics_post_fit(
       self, client_manager: ClientManager, timeout: Optional[float]
    ) -> None:
       """Get analytics from all clients.

       Arguments:
           client_manager: ClientManager. The client manager containing all currently connected clients.
           timeout: Optional[float]. timeout for client reply.

       Returns:
          Nothing.

       Use this function to run federated analytics:
       federated_analytics(
           self, query: str, config: Config, client_manager: ClientManager, timeout: Optional[float],
           aggregate_fn: Optional[AggregationFn],
           use_homomorphic_encryption: Optional[bool] = False,
       ) -> Tuple[int, Dict[str, Scalar]]
       """

    def __query_client(self,
        client: ClientProxy, ins: AnalyticsIns, timeout: Optional[float]
    ) -> Tuple[ClientProxy, AnalyticsRes]:
        """Refine parameters on a single client."""
        analytics_res = client.query_analytics(ins, timeout=timeout)
        return client, analytics_res

    def __query_clients(self,
        client_instructions: List[Tuple[ClientProxy, AnalyticsIns]], timeout: Optional[float]
    ) -> Tuple[List[Tuple[ClientProxy, AnalyticsRes]], List[BaseException]]:
        """Get analytics data concurrently on all selected clients."""
        with concurrent.futures.ThreadPoolExecutor(max_workers=self.max_workers) as executor:
            submitted_fs = {
                executor.submit(self.__query_client, client_proxy, ins, timeout)
                for client_proxy, ins in client_instructions
            }
            finished_fs, _ = concurrent.futures.wait(
                fs=submitted_fs,
                timeout=None,  # Handled in the respective communication stack
            )

        # Gather results
        results: List[Tuple[ClientProxy, AnalyticsRes]] = []
        failures: List[BaseException] = []
        for future in finished_fs:
            failure = future.exception()
            if failure is not None:
                failures.append(failure)
            else:
                # Success case
                result = future.result()
                results.append(result)
        return results, failures

    def __query_clients_in_circle(self,
        client_instructions: List[Tuple[ClientProxy, AnalyticsIns]],
        aggregate_fn: AggregationFn,
        timeout: Optional[float]
    ) -> Tuple[Tuple[ClientProxy, AnalyticsRes], List[BaseException]]:
        """Get analytics data concurrently on all selected clients."""

        log(DEBUG, "Homomorphic Encryption Requested")
        ins = client_instructions[0][1]
        ins.aggregate_function = get_aggregate_fn_enum_val(aggregate_fn)
        failures: List[BaseException] = []
        # first ins has no encrypted number it will be added by the first client
        clientid=0
        for client_proxy, _ in client_instructions:
            try:
                # log(DEBUG, "circular call for clients: id %s", clientid)
                _, tmpresults = self.__query_client(client_proxy, ins, timeout)
                ins.encrypted_number = tmpresults.encrypted_number # update number
                ins.num_examples = tmpresults.num_examples
                # results.append(tmpresults)
            except BaseException as exception:
                failures.append(exception)
            clientid += 1

        # decrypt summed things on first client, where it was encrypted:
        # log(DEBUG, "decrypt on client 0")
        try:
            ins.query = "decrypt"
            _, result = self.__query_client(client_instructions[0][0], ins, timeout)
        except BaseException as exception:
            failures.append(exception)

        # log(DEBUG,"results: %s; failures: %s", results, failures)
        return result, failures


    def __aggregate_analytics(
        self,
        results: List[Tuple[ClientProxy, AnalyticsRes]],
        failures: List[BaseException],
        aggregate_fn: AggregationFn, # Callable[[List[Tuple[int, Metrics]]], Metrics]
    ) -> AnalyticsRes:
        if not results:
            log(DEBUG, "ERROR: no results received")
            return AnalyticsRes(0, [], [])
        # Do not aggregate if there are failures and failures are not accepted
        if not self.strategy.accept_failures and failures:
            log(DEBUG, "ERROR: some failures occured")
            return AnalyticsRes(0, [], [])

        # Aggregate custom metrics if aggregation fn was provided
        return aggregate_fn([res for _, res in results])


    def federated_analytics(
        self, query: str, config: Config, client_manager: ClientManager,
        timeout: Optional[float],
        aggregate_fn: AggregationFn = sum_aggregate_fn,
        use_homomorphic_encryption: bool = False,
    ) -> AnalyticsRes:
        """Run federated analytics query on all clients.

        Parameters
        ----------
        query : str
            String that identifies the type of query reqeusted for federated analytics
        config : Config
            Config containing data needed for the federated analytics requested.
        client_manager : ClientManager
            ClientManager containing all clients currently connected.
        aggregate_fn : AggregationFn
            Aggregation Function to call on all results from all clients.
            Currently available: sum_aggregate_fn, concatenate_aggregate_fn, column_sum_aggregate_fn
            Default: sum_aggregate_fn
        use_homomorphic_encryption : Optional[bool]
            Use homomorphic encryption to calculate the data. Only works with a single
            value in the result (part of AnalyticsRes).
            Default: False
        timeout : Optional[float]
            Timeout for client replies.

        Returns
        -------
        AnalyticsRes
            Contains the number of samples used in the query for federated analytics
            (num_examples) and the results itself. The result is a list Scalars.
            WARNING: The result should only contain a single value of homomorphic encryption
            is requested.
            WARNING: Do not use 'encrypted_number' unless you know how this works.
        """

        if use_homomorphic_encryption and not aggregate_fn:
            raise Exception("no aggregate function provided for federated analytics")

        # wait for all clients to be connected
        client_manager.wait_for(self.strategy.min_available_clients)
        # TODO when to decrypt encrypted data?, before aggregate?
        analytics_ins = AnalyticsIns(query=query, config=config, encryption=use_homomorphic_encryption, encrypted_number=[])

        clients = list(client_manager.all().values())
        client_instructions = [(client, analytics_ins) for client in clients]
        log(DEBUG, "querying %s clients for %s", len(client_manager.all()), query)
        if use_homomorphic_encryption:
            res, failures = self.__query_clients_in_circle(client_instructions, aggregate_fn, timeout)
        else:
            results, failures = self.__query_clients(client_instructions, timeout)
            res = self.__aggregate_analytics(results, failures, aggregate_fn)

        if(len(res.result) == 0):
            raise Exception("result is empty")

        return res

with import <nixpkgs> { };

python310Packages.buildPythonPackage {
	pname = "flwr";
	version = "0.1";

	src = ./.;
	doCheck = false;
	propagatedBuildInputs = with python310Packages; [
		numpy
		pandas
		scikit-learn
		protobuf
		grpcio
		phe
	];
}

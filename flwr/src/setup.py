import re
from setuptools import find_packages, setup

PACKAGE_NAME = 'flwr'
SOURCE_DIRECTORY = '.'

if __name__ == '__main__':
    setup(
        name=PACKAGE_NAME,
        packages=['flwr','iterators'],
        package_dir={'flwr': SOURCE_DIRECTORY, 'iterators': SOURCE_DIRECTORY}
    )

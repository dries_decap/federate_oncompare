#!/bin/bash

help="commands:\n\
 -s|--server <num_clients> <max_rounds>\tstart the OnCompare server\n\
 -c|--client <datafile> <server>\tstart an OnCompare client with given input file as data [requires datafile]\n\
 -b|--bash\tstart an interactive bash shell\n\
 -h|--help\tshow help"

key="$1"
shift;
case $key in
    -h|--help)
      echo -e $help
      exit 0; # exit command
      ;;
    -b|--bash)
      cmd="/bin/bash"
      ;;
    -s|--server)
      if [ "$#" -ne 2 ]; then
        echo "--server requires 2 arguments: <num_clients> <max_rounds>"
        exit -2;
      fi
      echo "server hostname is $(hostname) [ip-addrs: $(hostname -I)]"
      CLIENTS="$1"
      shift # past argument
      ROUNDS="$1"
      shift # past argument
      cmd="python3 server.py $CLIENTS $ROUNDS"
      ;;
    -c|--client)
      if [ "$#" -ne 2 ]; then
        echo "--client requires 2 arguments: <datafile> <server>"
        exit -2;
      fi
      echo "client hostname is $(hostname) [ip-addrs: $(hostname -I)]"
      FILE="$1"
      shift # past argument
      if [ ! -f "data/$FILE" ]; then
        echo "File $FILE does not exist in data/, make sure the volume with the file is mapped to /opt/oncompare/data/"
        exit -1;
      fi
      SERVER="$1"
      shift # past argument
      cmd="python3 client.py /opt/oncompare/data/$FILE $SERVER"
      ;;
    *)    # unknown option
      echo "option $1 not available."
      echo -e $help
      ;;
esac

$cmd

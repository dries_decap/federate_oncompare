

from flwr.common.logger import log
from flwr.server.client_manager import ClientManager
import math
from typing import Dict, Tuple, Optional
from logging import DEBUG, INFO
from flwr.server.strategy import StrategyWithFederatedAnalyticsWrapper
from flwr.common import Scalar
from flwr.server.strategy import (sum_aggregate_fn, concatenate_aggregate_fn, column_sum_aggregate_fn)
import utils

class OnCompareStrategy(StrategyWithFederatedAnalyticsWrapper):
    def run_federated_analytics_pre_fit(
        self, client_manager: ClientManager, timeout: Optional[float]
    ) -> None:
        """Get analytics from all clients."""
        encryption = False
        config = {}
        # mean
        res = self.federated_analytics("sum", config, client_manager, timeout, column_sum_aggregate_fn, encryption)
        log(INFO, "sum result: %s", res)
        for i in range(0, len(res.result)):
            config["mean_"+str(i)] = res.result[i]/res.num_examples
            log(INFO, "mean_%s: %s", i, config["mean_"+str(i)])

        # std dev
        res = self.federated_analytics("sumofsquares", config, client_manager, timeout, column_sum_aggregate_fn, encryption)
        log(INFO, "sumofsquares result: %s", res)
        for i in range(0, len(res.result)):
            config["std_dev_"+str(i)] = math.sqrt(res.result[i]/res.num_examples )
            log(INFO, "std_dev_%s: %s", i, config["std_dev_"+str(i)])

        # rescale
        res = self.federated_analytics("rescale", config, client_manager, timeout)
        self.pre_config = config # could be done if elements are required in post-fit FA

    def run_federated_analytics_post_fit(
        self, client_manager: ClientManager, timeout: Optional[float]
    ) -> None:
        """Get analytics from all clients."""
        encryption = False  
        config = self.pre_config # can be used

        # get probabilities
        res = self.federated_analytics("probabilities_N1", config, client_manager, timeout, concatenate_aggregate_fn, encryption)
        probabilities_N1 = res.result
        log(DEBUG, "%s N1 probabilities", len(probabilities_N1)) #, probabilities_N1)
        # get probabilities
        res = self.federated_analytics("probabilities_N2", config, client_manager, timeout, concatenate_aggregate_fn, encryption)
        probabilities_N2 = res.result
        log(DEBUG, "%s N2 probabilities", len(probabilities_N2)) #, probabilities_N2)
        # get observations
        res = self.federated_analytics("observed_N1", config, client_manager, timeout, sum_aggregate_fn, encryption)
        observed_N1 = res.result[0]
        log(DEBUG, "observed_N1: %s", observed_N1)
        # get observations
        res = self.federated_analytics("observed_N2", config, client_manager, timeout, sum_aggregate_fn, encryption)
        observed_N2 = res.result[0]
        log(DEBUG, "observed_N2: %s", observed_N2)

        pval = utils.getPVal(observed_N1, observed_N2, probabilities_N1, probabilities_N2)
        log(INFO, "OnCompare pVal is %s", pval)

import warnings
import flwr as fl
import numpy as np
import sys

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import log_loss

from flwr.common.logger import log
from logging import DEBUG, INFO
from typing import Dict
from flwr.common import Config, Properties, Scalar, AnalyticsIns, AnalyticsRes, Parameters, EncryptedNumber
from flwr.client.numpy_client import NumPyClientWrapper
from phe import paillier

# from flwr.common import EvaluateIns, EvaluateRes, , Parameters, Scalar, Config, MetricsAggregationFn

import utils
# read arguments -> hospital id for loading corresponding data

EXCEPTION_MESSAGE_MISSING_CONFIG_PARAMETER = """
ClientWithFederatedAnalytics.query_analytics does not have the required
data in the config variable.
"""
EXCEPTION_MESSAGE_UNSUPPORTED_QUERY = """
ClientWithFederatedAnalytics.query_analytics Unsupported query type used.
"""


# Define Flower client
class OnCompareClient(fl.client.NumPyClient):
    def __init__(self, datafile):
        # log(INFO, "pub key: %s", self.pub_key)
        # load dataset
        self.X_unscaled, self.y, self.X_unscaled_N1, self.X_unscaled_N2, self.observed_N1, self.observed_N2 = utils.load_dataset(datafile)
        self.X = None
        # Create LogisticRegression Model
        self.model = LogisticRegression(
            penalty='none',
            max_iter=1,  # local epoch
            warm_start=True,  # prevent refreshing weights when fitting
        )
        # Setting initial parameters, akin to model.compile for keras models
        utils.set_initial_params(self.model)

    def get_parameters(self):  # type: ignore
        return utils.get_model_parameters(self.model)

    def fit(self, parameters, config):  # type: ignore
        utils.set_model_params(self.model, parameters)
        log(DEBUG, "parameters used: %s", parameters)
        # Ignore convergence failure due to low local epochs
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.model.fit(self.X, self.y)
        # log(DEBUG, "Training finished for round %s", config['rnd'])
        return utils.get_model_parameters(self.model), len(self.X), {}

    def evaluate(self, parameters, config):  # type: ignore
        utils.set_model_params(self.model, parameters)
        loss = log_loss(self.y, self.model.predict_proba(self.X))
        accuracy = self.model.score(self.X, self.y)
        print(f"Loss is {loss} and accuracy is {accuracy}")
        return loss, len(self.X), {}

    def rescale(self, config):
        i = 0
        self.X = ((self.X_unscaled[:, i] - config["mean_" + str(i)] ) / config["std_dev_" + str(i)]).reshape(-1, 1)
        self.X_N1 = ((self.X_unscaled_N1[:, i] - config["mean_" + str(i)] ) / config["std_dev_" + str(i)]).reshape(-1, 1)
        self.X_N2 = ((self.X_unscaled_N2[:, i] - config["mean_" + str(i)] ) / config["std_dev_" + str(i)]).reshape(-1, 1)
        for i in range(1, self.X_unscaled.shape[1]):
            self.X = np.append(self.X, ((self.X_unscaled[:, i] - config["mean_" + str(i)] ) / config["std_dev_" + str(i)]).reshape(-1, 1), axis=1)
            self.X_N1 = np.append(self.X_N1, ((self.X_unscaled_N1[:, i] - config["mean_" + str(i)] ) / config["std_dev_" + str(i)]).reshape(-1, 1), axis=1)
            self.X_N2 = np.append(self.X_N2, ((self.X_unscaled_N2[:, i] - config["mean_" + str(i)] ) / config["std_dev_" + str(i)]).reshape(-1, 1), axis=1)

    def get_probabilities_N1(self):
        return self.model.predict_proba(self.X_N1)[:, 1]
    def get_probabilities_N2(self):
        return self.model.predict_proba(self.X_N2)[:, 1]

    def query_analytics(self, ins: AnalyticsIns) -> AnalyticsRes:
        # log(DEBUG, "analytics query received: %s", ins)
        data = []
        # select query:
        match ins.query:
            case "sum": # can be encrypted as its only 1 value
                data.extend(self.X_unscaled.sum(axis=0).flatten().tolist())
            case "sumofsquares": # can be encrypted as its only 1 value
                for i in range(0, self.X_unscaled.shape[1]):
                    if "mean_" + str(i) not in ins.config:
                        raise Exception(EXCEPTION_MESSAGE_MISSING_CONFIG_PARAMETER)
                    data.append(np.sum(np.power((self.X_unscaled[:, i] - ins.config['mean_' + str(i)]), 2)).item())
            case "rescale": # can be encrypted since it's 1 value but doesn't matter since value is always 1
                for i in range(0, self.X_unscaled.shape[1]):
                    if "mean_" + str(i) not in ins.config or "std_dev_" + str(i) not in ins.config:
                        raise Exception(EXCEPTION_MESSAGE_MISSING_CONFIG_PARAMETER)
                self.rescale(ins.config)
                data.append(1) # 1 means complete
            case "probabilities_N1": # can be encrypted since it has multiple values added to the result list
                data.extend(self.get_probabilities_N1())
            case "probabilities_N2": # can be encrypted since it has multiple values added to the result list
                data.extend(self.get_probabilities_N2())
            case "observed_N1": # can be encrypted as its only 1 value
                data.append(self.observed_N1)
            case "observed_N2": # can be encrypted as its only 1 value
                data.append(self.observed_N2)
            case _:
                raise Exception(EXCEPTION_MESSAGE_UNSUPPORTED_QUERY)

        # log(DEBUG, "analytics data to send: %s", data)
        return AnalyticsRes(
            num_examples=len(self.X_unscaled),
            result=data,
            encrypted_number=[]
        )

def start_client(datafile, server):
    # Start Flower client
    fl.client.start_numpy_client(server + ":8080", client=OnCompareClient(datafile))

if __name__ == "__main__":

    if len(sys.argv) != 3:
        print("provide a data file and a server to connect to.")
        sys.exit(-1);
    datafile=sys.argv[1]
    server=sys.argv[2]
    start_client(datafile, server)

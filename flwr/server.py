
import flwr as fl
import utils
from sklearn.metrics import log_loss
from sklearn.linear_model import LogisticRegression
from typing import Dict
from  oncompare_strategy import OnCompareStrategy
import sys

def fit_round(rnd: int) -> Dict: # can be used to send variables to client par example
    """Send round number to client."""
    # print("round " + str(rnd))
    return {"rnd": rnd}

def start_server(max_rounds, clients):
    model = LogisticRegression()
    utils.set_initial_params(model)
    strategy = OnCompareStrategy(fl.server.strategy.FedAvg(
        min_available_clients=clients,
        min_fit_clients=clients,
        min_eval_clients=clients,
        # eval_fn=get_eval_fn(model),
        # on_fit_config_fn=fit_round
        )
    )
    fl.server.start_server(
        server_address="0.0.0.0:8080",
        strategy=strategy,
        config={
            "num_rounds": max_rounds,
            "loss_convergence_threshold": 0.00001 # minimum difference between loss in two iterations to assume convergence
        }
    )


# Start Flower server for five rounds of federated learning
if __name__ == "__main__":

    if len(sys.argv) != 3:
        print("provide the number of clients and max number of rounds for logistic regression.")
        sys.exit(-1);
    clients=int(sys.argv[1])
    max_rounds=int(sys.argv[2])

    start_server(max_rounds, clients);

# FLWR extended
The flwr package has been extended to support Federated Analytics in a step before the Federated Learning and a second time after the Federated Learning.
Additionally, Full Homomorphic Encryption (FHE) has been added for data summed over several nodes. This is only supported when a single number is requested
for the Federated Analytics query, i.e. sum. It does not support multiple values (i.e. a list) as it's strength lies in operations on encrypted data.


## Dependencies/Libraries
### phe python package
This package is needed on all nodes to use Full Homomorphic Encryption in federated analytics steps. This can increment an encrypted number with local data.
```bash
# install phe library for all users
pip3 install dataclasses phe grpcio protobuf google-api-python-client iterators

```

## Usage:
### client
A client application needs to be created that implements a flwr client (numpy_client here). This class needs to override several function related to the
Federated Learning:

```python
def get_parameters(self):  
def fit(self, parameters, config):
def evaluate(self, parameters, config):  
```

This is the same as described [here](https://flower.dev/docs/quickstart-scikitlearn.html)

For the Federated Analytics steps, the client needs to be able to return the relevant data, this is provided by overriding this function:

```python
def query_analytics(self, ins: AnalyticsIns) -> AnalyticsRes:
# the result contains the number of samples and a list of data to return. Encryption is only supported on list with just a single value.
```

A query string is provided in AnalyticsIns that identifies the Federated Analytics data requested. Optionally data can be added in the config parameter of AnalyticsIns.

### server
A server application is started which will start a given strategy, this is also based on [this tutorial](https://flower.dev/docs/quickstart-scikitlearn.html).
For Federated Analytics a custom strategy needs to be implemented. This new strategy needs to extend ``StrategyWithFederatedAnalyticsWrapper`` and override these functions:

```python
def run_federated_analytics_pre_fit(self, client_manager: ClientManager, timeout: Optional[float]) -> Optional[Dict[str, Scalar]]:
def run_federated_analytics_post_fit(self, client_manager: ClientManager, timeout: Optional[float]) -> Tuple[int, Dict[str, Scalar]]:
```

This is a wrapper class, that takes a regular Strategy as input, therefore any existing strategies can be used with this approach.

In these functions Federated Analytics can be requested by calling this function:

```python
config = {}
res = self.federated_analytics(query, config, client_manager, timeout, sum_aggregate_fn, use_homomorphic_encryption = use_encryption)
# the first parameter is the query, in this case the sum. This should be supported in the clients 'query_analytics' function.
# config  can contain additional information
# possible aggregate functions are sum_aggregate_fn and concatenate_aggregate_fn to respectively sum all values and concatenate all values in a list
# the last variable is a boolean to enable or disable encryption. This is only supported for queries that return a single value.
```

### federated analytics example
As an example the mean and standard deviation over all data can be achieved like this:
```python
# the strategy implementation (only relevant function is shown):
class ExampleStrategy(fl.server.strategy.StrategyWithFederatedAnalyticsWrapper):
    def run_federated_analytics_pre_fit(
        self, client_manager: ClientManager, timeout: Optional[float]
    ) -> None:
      encryption = True
      config = {}
      # mean
      res = self.federated_analytics("sum", config, client_manager, timeout, sum_aggregate_fn, use_homomorphic_encryption = encryption)
      log(INFO, "sum result: %s", res)
      config["mean"] = res[1][0] / res[0]
      log(INFO, "mean: %s", config["mean"])

      # std dev
      res = self.federated_analytics("sumofsquares", config, client_manager, timeout, sum_aggregate_fn, use_homomorphic_encryption = encryption)
      log(INFO, "sumofsquares result: %s", res)
      config["std_dev"] = math.sqrt(res[1][0] / res[0])
      log(INFO, "std_dev: %s", config["std_dev"])

      # rescale
      res = self.federated_analytics("rescale", config, client_manager, timeout, sum_aggregate_fn)
      self.pre_config = config # could be set if elements are required in post-fit FA, to avoid recalculation


# client implementation (only relevant functions are shown)
class ExampleClient(fl.client.NumPyClient):
    def rescale(self, config):
        self.X = (self.X_unscaled - config['mean'] ) / config['std_dev']

    def query_analytics(self, ins: AnalyticsIns) -> AnalyticsRes:
        # log(DEBUG, "analytics query received: %s", ins)
        data = []
        # select query:
        match ins.query:
            case "sum": # can be encrypted as its only 1 value
                data.append(np.sum(self.X_unscaled).item()) # .item() called to have python values rather than numpy values, which are not supported
            case "sumofsquares": # can be encrypted as its only 1 value
                if "mean" not in ins.config:
                    raise Exception("EXCEPTION_MISSING_CONFIG_PARAMETER")
                data.append(np.sum(np.power((self.X_unscaled - ins.config['mean']), 2)).item()) # .item() called to have python values rather than numpy values, which are not supported
            case "rescale": # can be encrypted since it's 1 value but doesn't matter since value is always 1
                if "mean" not in ins.config or "std_dev" not in ins.config:
                    raise Exception("EXCEPTION_MISSING_CONFIG_PARAMETER")
                self.rescale(ins.config)
                data.append(1) # 1 means complete
            case _:
                raise Exception("EXCEPTION_UNSUPPORTED_QUERY")
        return AnalyticsRes(num_examples=len(self.X_unscaled), result=data)
```

As seen above, this can be used to call a function on local data, i.e. rescaling the data based on the mean and standard deviation. Without actually returning relevant data.
FHE can be enabled or disabled with a single boolean, no manual code is needed for this. The encryption is enabled automatically as long as only a single value is provided in the result list.
If the list does not contain exactly one value it will raise an exception which in turn will provide a crash on the server application caused by empty results. The exception is shown in the client applications.

> **_NOTE:_**  The full code for the OnCompare example can be found in ``client.py``, ``server.py``, ``oncompare_strategy.py`` and ``utils.py``.


## Proto3 files
### recompiling the proto files
This is only required when the ``flwr/proto/flwr/proto/transport.proto`` has been changed.
This depends on the directory structure this is run in (configured in ``flwr/flwr_tool/protoc.py``).

```bash
# this directory should contain these folders: src/flwr/, proto/, flwr_tool/

# make sure libraries are installed:
sudo pip3 install grpcio-tool grpcio protobuf

# create protocols:
python3 -m flwr_tool.protoc
```


## Possible extensions?

- Add encryption for data not supported by FHE.
- FHE on lists, create a sum per element
- other operations with FHE -> custom FHE aggregation function?
-

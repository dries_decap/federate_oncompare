from typing import Tuple, Union, List
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler

XY = Tuple[np.ndarray, np.ndarray]
LogRegParams = Union[XY, Tuple[np.ndarray]]
# Dataset = Tuple[XY, XY]

data_fields = ["TMB"] #, "RND", "TP53"]


# LOG REG MANAGEMENT
def get_model_parameters(model: LogisticRegression) -> LogRegParams:
    """Returns the paramters of a sklearn LogisticRegression model."""
    if model.fit_intercept:
        params = [model.coef_, model.intercept_]
    else:
        params = [model.coef_,]
    return params


def set_model_params(
    model: LogisticRegression, params: LogRegParams
) -> LogisticRegression:
    """Sets the parameters of a sklean LogisticRegression model."""
    model.coef_ = params[0]
    if model.fit_intercept:
        model.intercept_ = params[1]
    return model


def set_initial_params(model: LogisticRegression):
    """Sets initial parameters as zeros Required since model params are
    uninitialized until model.fit is called.
    But server asks for initial parameters from clients at launch. Refer
    to sklearn.linear_model.LogisticRegression documentation for more
    information.
    """
    n_classes = 1  # oncompare dataset has only 1 class
    n_features = len(data_fields)  # Number of features in dataset
    model.classes_ = np.array([i for i in range(n_classes)])

    model.coef_ = np.zeros((n_classes, n_features))
    if model.fit_intercept:
        model.intercept_ = np.zeros((n_classes,))



# DATA MANAGEMENT
def prep_data(mut_data1, mut_data2, covariates1, covariates2, scale):
    # # should be dataframes...
    # if isinstance(mut_data1, pd.Series):
    #     mut_data1 = mut_data1.to_frame()
    # if isinstance(mut_data2, pd.Series):
    #     mut_data2 = mut_data2.to_frame()
    # if isinstance(covariates1, pd.Series):
    #     covariates1 = covariates1.to_frame()
    # if isinstance(covariates2, pd.Series):
    #     covariates2 = covariates2.to_frame()

    common_genes = np.intersect1d(mut_data1.columns.values, mut_data2.columns.values)
    assert len(common_genes) > 0, "Please assure that the mut series have matching column labels"
    mut_data1, mut_data2 = mut_data1[common_genes], mut_data2[common_genes]

    # remove NaNs in case there are present:
    covariates2 = covariates2.dropna(axis=0)
    covariates1 = covariates1.dropna(axis=0)

    # find common samples between mut_data1 and clin_data1
    common_samples1 = np.intersect1d(mut_data1.index.values, covariates1.index.values)
    assert len(common_samples1) > 0, "The sample ids are not matching for the first dataset." \
                                     "Please assure that both the clinical data and the covariates" \
                                     " have the sample ids as their index."
    mut_data1, covariates1 = mut_data1.loc[common_samples1], covariates1.loc[common_samples1]
    # same for data2
    common_samples2 = np.intersect1d(mut_data2.index.values, covariates2.index.values)
    assert len(common_samples2) > 0, "The sample ids are not matching for the first dataset." \
                                     "Please assure that both the clinical data and the covariates" \
                                     " have the sample ids as their index."

    mut_data2, covariates2 = mut_data2.loc[common_samples2], covariates2.loc[common_samples2]

    assert len(np.intersect1d(covariates1.columns.values, covariates2.columns.values)) > 0, "Please make sure that the" \
                                                                     " columns of the covariates have identical names."

    # merge datasets
    mut_data = pd.concat([mut_data1, mut_data2], axis=0)
    covariates = pd.concat([covariates1, covariates2], axis=0)

    # prep data
    common_samples = np.intersect1d(mut_data.index.values, covariates.index.values)

# TODO clean up this code to not keep converting between pd.series and pd.dataframes

    # # get X, Y from mut_data/covariates
    # if isinstance(covariates, pd.Series):  # for a single covariate, the user can provide data as a pd.Series
    #     covariates = covariates.to_frame()
    # if isinstance(mut_data, pd.Series):  # for a single covariate, the user can provide data as a pd.Series
    #     mut_data = mut_data.to_frame()

    y = mut_data.loc[common_samples].values #.flatten()
    if len(np.unique(y)) == 1:
        raise Exception("Mutation data contains no mutations or all samples are mutated.")

    X = covariates.loc[common_samples].values

    return covariates.values, y, covariates1.values, covariates2.values, np.sum(mut_data1.values > 0).item(), np.sum(mut_data2.values > 0).item()

def scaleX(X_unscaled, config):
    return ( X_unscaled * config['mean'] ) / config['std_dev']

def load_dataset(path, scale = True): # -> Dataset:
    """Loads dataset from path
    """
    df = pd.read_csv(path)
    pos_group = df.loc[df.LNI == 1]
    neg_group = df.loc[df.LNI == 0]
    mutation_status_negatives = neg_group[["TP53"]]
    mutation_status_positives = pos_group[["TP53"]]
    covariates_negatives = neg_group[data_fields]
    covariates_positives = pos_group[data_fields]
    # return mutation_status_negatives, mutation_status_positives, covariates_negatives, covariates_positives
    return prep_data(mutation_status_negatives, mutation_status_positives, covariates_negatives, covariates_positives, scale)


# OnCompare

def poisson_binom_pmf(probs):
    pmf = np.zeros(len(probs) + 1)

    pmf[0] = 1. - probs[0]
    pmf[1] = probs[0]

    for prob in probs[1:]:
        pmf = ((1-prob) * pmf + np.roll(pmf, 1) * prob)

    return pmf

def getPVal(observed1, observed2, probs1, probs2):
    #     print(f"observed: {observed1} and {observed2}")
    #     print(f"len probs: {len(probs1)} and {len(probs2)}")
    #     print(probs2)

    large_number = 1e12  # needs to be larger than a typical dataset
    tail='two-sided' # default value
    # ratio
    if (observed1 < 1) and (observed2 < 1):
        if tail != "two-sided":
            pval = 0.5
        else:
            pval = 1.
    else:
        if observed2 < 1:
            observed_ratio = observed1 * large_number
        elif observed1 < 1:
            observed_ratio = 1. / (observed2 * large_number)
        else:
            observed_ratio = 1. * observed1 / observed2

        nsamples1, nsamples2 = len(probs1), len(probs2)
        set1_range, set2_range = np.arange(nsamples1 + 1, dtype=float), np.arange(nsamples2 + 1, dtype=float)
        set2_range[0], set1_range[0] = 1./large_number, 1./large_number

        prob_mask = set1_range[..., None] / set2_range[None, ...]
        # prob_mask[0, :] = 0.
        prob_mask[0, 0] = 1.

        pmf_set1 = poisson_binom_pmf(probs1) # was without .values but gives an error then...
        pmf_set2 = poisson_binom_pmf(probs2) # was without .values but gives an error then...

        joint_pmf = np.outer(pmf_set1, pmf_set2)

        if tail == 'left':
            pval = joint_pmf[prob_mask <= observed_ratio].sum()

        elif tail == 'right':
            pval = joint_pmf[prob_mask >= observed_ratio].sum()

        else:
            pval_left = joint_pmf[prob_mask <= observed_ratio].sum()
            pval_right = joint_pmf[prob_mask >= observed_ratio].sum()
            pval = np.minimum(2 * np.minimum(pval_left, pval_right), 1.)

    # print(f"pval is {pval}")
    return pval

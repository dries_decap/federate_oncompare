import numpy as np # linear algebra
import pandas as pd
from sklearn.preprocessing import StandardScaler
import sys

np.random.seed(42)
# from sklearn.base import BaseEstimator, ClassifierMixin
class FederatedError(Exception):
    """General Federate Error"""
    pass
class SecurityError(FederatedError):
    """Raised when query is not secure"""
    pass
class NumberOfFeaturesError(FederatedError):
    """number of features is not the same on all federated samples"""
    pass
class InsufficientDataError(FederatedError):
    """Insufficient local data to remain anonymous"""
    pass
class NoMutationOrAllMutationError(FederatedError):
    """Mutation data contains no mutations or all samples are mutated"""
    pass
class InvalidQueryError(FederatedError):
    """Invalid query requested"""
    pass
class NoCommonSamplesError(FederatedError):
    """No common samples found between mutation status and covariates"""
    pass
class NoMatchingCovariatesError(FederatedError):
    """No common samples found between mutation covariates of the two groups"""
    pass


class federatedHospitalWithSecurityLayer: # simulation
    def __init__(self, path):
        self.__minimumDataForAnonymousData = 10
        self.__validQueries = ["deltas", "get_n_samples", "prep", "get_n_features", "predict_proba_pos", "predict_proba_neg", "count_observations_pos", "count_observations_neg", "get_sum", "get_sum_in_squares", "scale_data"]
        self.__invalidDebugQueries = ["show_samples"]
        self.__collectDataQueries = ["get_n_samples", "deltas", "get_n_features", "predict_proba_pos", "predict_proba_neg", "count_observations_pos", "count_observations_neg"]
        self.__DEBUG_MODE = False;
        self.__path = path
    
    # private functions
    def __getAndFilterData(self):
        # read data and create dataframes
        df = pd.read_csv(self.__path)
        pos_group = df.loc[df.LNI == 1]
        neg_group = df.loc[df.LNI == 0]
        self.__mutation_status_negatives = pd.Series(neg_group["TP53"]).to_frame()
        self.__mutation_status_positives = pd.Series(pos_group["TP53"]).to_frame()
        self.__covariates_negatives = pd.Series(neg_group["TMB"]).to_frame()
        self.__covariates_positives = pd.Series(pos_group["TMB"]).to_frame()
        # filter data
        # common genes, i.e. gene columns present in all samples
        self.__common_genes = np.intersect1d(self.__mutation_status_negatives.columns.values, self.__mutation_status_positives.columns.values)
        if not len(self.__common_genes) > 0:
            raise NoMatchingCovariatesError
        self.__mutation_status_negatives = self.__mutation_status_negatives[self.__common_genes]
        self.__mutation_status_positives = self.__mutation_status_positives[self.__common_genes]
        # remove NaNs in case there are present:
        self.__covariates_negatives = self.__covariates_negatives.dropna(axis=0)
        self.__covariates_positives = self.__covariates_positives.dropna(axis=0)
        # match with mutation_status samples
        self.__common_samples_negatives = np.intersect1d(self.__mutation_status_negatives.index.values, self.__covariates_negatives.index.values)
        if not len(self.__common_samples_negatives) > 0:
            raise NoCommonSamplesError
        self.__mutation_status_negatives = self.__mutation_status_negatives.loc[self.__common_samples_negatives]
        self.__covariates_negatives = self.__covariates_negatives.loc[self.__common_samples_negatives]        
        
        self.__common_samples_positives = np.intersect1d(self.__mutation_status_positives.index.values, self.__covariates_positives.index.values)
        if not len(self.__common_samples_positives) > 0:
            raise NoCommonSamplesError
        self.__mutation_status_positives = self.__mutation_status_positives.loc[self.__common_samples_positives]
        self.__covariates_positives = self.__covariates_positives.loc[self.__common_samples_positives]

        if not len(np.intersect1d(self.__covariates_negatives.columns.values, self.__covariates_positives.columns.values)) > 0:
            raise NoMatchingCovariatesError
    
    def __isQueryValid(self, query):
        if not( query in self.__validQueries or self.__DEBUG_MODE and query in self.__invalidDebugQueries):
            return False
        else:
            if query in self.__invalidDebugQueries:
                print(f"WARNING! invalid DEBUG query requested: {query}")
            return True
    def __isCollectingData(self, query):
        if query not in self.__collectDataQueries:
            return False
        else:
            return True
        
    def __getLocalDataCountForQuery(self, query = None): # might depend on query... like filter data or something/ merge with prep? aka filter function or something
        return len(self.__localX)
    
    def __getLogits(self, weights):
        # logits = log(odds) = X@W + b
        return self.__localX @ weights['coef'] + weights['intercept']
    
    def __predictProbabilities(self, weights):
        logits = self.__getLogits(weights)
        return 1 / (1 + np.exp(-logits))
    
    def __isSecure(self, query):
        if not self.__isQueryValid(query):
            raise InvalidQueryError
        if self.__isCollectingData(query) and self.__getLocalDataCountForQuery(query) < self.__minimumDataForAnonymousData:
            raise InsufficientDataError
        if "raw" in query: # dummy to filter queries on raw data...
            raise SecurityError
    
    # queries
    def __queryDeltasForLogReg(self, data):
        y_proba = self.__predictProbabilities(data)
        loss = - np.sum(self.__localy * np.log(y_proba) + (1 - self.__localy) * np.log(1 - y_proba)) # / self.__n_samples
        # Gradient of loss w.r.t weights.
        dw = self.__localX.T @ (y_proba - self.__localy) # / self.__n_samples
        # Gradient of loss w.r.t bias.
        db = np.sum((y_proba - self.__localy)) # / self.__n_samples
        return loss, dw, db
    
    def __queryPrepLocalData(self):
        self.__getAndFilterData() 
        self.__localy = pd.concat([self.__mutation_status_negatives, self.__mutation_status_positives], axis=0)
        self.__localX = pd.concat([self.__covariates_negatives, self.__covariates_positives], axis=0)
        if isinstance(self.__localX, pd.Series):  # for a single covariate, the user can provide data as a pd.Series
            self.__localX = self.__localX.to_frame()
        if isinstance(self.__localy, pd.Series):  # for a single covariate, the user can provide data as a pd.Series
            self.__localX = self.__localy.to_frame()

        self.__common_samples = np.intersect1d(self.__localy.index.values, self.__localX.index.values)
        self.__localy = self.__localy.loc[self.__common_samples].values
        if len(np.unique(self.__localy)) == 1:
            raise NoMutationOrAllMutationError 
        self.__localX = self.__localX.loc[self.__common_samples].values
            
        self.__n_samples = self.__localX.shape[0]   # number of samples
        self.__n_features = self.__localX.shape[1]  # number of features
            
    def __queryGetSumOfData(self):
        return np.sum(self.__localX)
        
    def __queryGetSumInSquaresOfData(self, data):
#         sum_of_squares = 0
#         for x in self.__localX:
#             sum_of_squares +=  (x - data['mean']) ** 2
#         return sum_of_squares
        return np.sum(np.power((self.__localX - data['mean']), 2))
                
    def __queryScaleData(self, data):
        self.__localX = (self.__localX - data['mean'] ) / data['std_dev']
        
    def __queryGetNumFeatures(self):
        return self.__n_features
    
    def __queryPredictProba(self, data):
        return pd.Series(self.__predictProbabilities(data)[:, 0], index=self.__common_samples)
    
    def __queryPredictProbaPos(self, data):
        return self.__queryPredictProba(data).iloc[len(self.__common_samples_negatives):]
    
    def __queryPredictProbaNeg(self, data):
        return self.__queryPredictProba(data).iloc[0:len(self.__common_samples_negatives)]
 
    def __queryGetNSamples(self):
        return self.__getLocalDataCountForQuery()
    
    def __queryShowSamples(self):
        print(self.__localX)    
    
    def __queryCountObservationsPos(self):
        return np.sum(self.__mutation_status_positives.values > 0) 
        
    def __queryCountObservationsNeg(self):
        return np.sum(self.__mutation_status_negatives.values > 0) 
    
        
    # run query as a service
    def runQuery(self, query, data = None):
        try:
            self.__isSecure(query)
            # select and execute query:
            if query == "deltas":
                return self.__queryDeltasForLogReg(data)
            elif query == "prep":
                self.__queryPrepLocalData()
            elif query == "get_n_samples":
                return self.__queryGetNSamples()
            elif query == "get_sum":
                return self.__queryGetSumOfData()
            elif query == "get_sum_in_squares":
                return self.__queryGetSumInSquaresOfData(data)
            elif query == "scale_data":
                return self.__queryScaleData(data)
            elif query == "show_samples": # invalid debug query
                self.__queryShowSamples()
            elif query == "get_n_features":
                return self.__queryGetNumFeatures()
            elif query == "predict_proba_pos":
                return self.__queryPredictProbaPos(data)
            elif query == "predict_proba_neg":
                return self.__queryPredictProbaNeg(data)
            elif query == "count_observations_pos":
                return self.__queryCountObservationsPos()
            elif query == "count_observations_neg":
                return self.__queryCountObservationsNeg()
            else:
                raise InvalidQueryError
        except FederatedError:
            raise            

class FederatedLogisticRegression:
    def __init__(self, federatedHospitals, scale=True):
        self.__verbose = 0
        self.__params = {}
        self.__n_samples = 0
        self.__params['mean'] = 0
        for hospital in federatedHospitals:
            hospital.runQuery("prep") 
#             hospital.runQuery("show_samples") # DEBUG
            self.__n_samples += hospital.runQuery("get_n_samples")
            if(scale):
                self.__params['mean'] += hospital.runQuery("get_sum")
        if(scale):
            self.__params['mean'] = self.__params['mean'] / self.__n_samples

        # sanity check: check if n_features is the same for all -> if not can get a intersect and only take those columns
        self.n_features = federatedHospitals[0].runQuery("get_n_features")
        for hospital in federatedHospitals:
            if hospital.runQuery("get_n_features") != self.n_features:
                raise NumberOfFeaturesError
                
        # rescale data (improves log reg performance) but should happen on all data equally
        if(scale):
            sum_in_square = 0
            for hospital in federatedHospitals:
                sum_in_square += hospital.runQuery("get_sum_in_squares", self.__params) 
            self.__params['std_dev'] = np.sqrt( sum_in_square / self.__n_samples )
            for hospital in federatedHospitals:
                hospital.runQuery("scale_data", self.__params) 
            
        # init weights
        self.__params['coef'] = np.zeros((self.n_features, 1)) #np.random.randn(self.n_features, 1)
        self.__params['intercept'] = np.ones((1, 1))   #np.random.randn(1, 1)
        
        self.__convergence_threshold = 0.0001
        
        
    def __fit(self, federatedHospitals, learning_rate=0.05, iterations=1000):
#         print(f"Initial Coefficients: {self.params['intercept'][0]} & {self.params['coef'].T[0]}")
        prev_loss = None
        for i in range(1, iterations + 1):
            dW = 0
            db = 0
            loss = 0
            for hospital in federatedHospitals:
                # get new deltas with from new weights
                hospital_local_loss, hospital_local_dW, hospital_local_db = hospital.runQuery("deltas", self.__params)
                loss += hospital_local_loss
                dW += hospital_local_dW 
                db += hospital_local_db 
                        
            loss = loss / self.__n_samples
            dW = dW / self.__n_samples
            db = db / self.__n_samples
            self.__params['coef'] -= (learning_rate * dW )
            self.__params['intercept'] -= (learning_rate * db )
            
            if self.__verbose and (i == 1 or i % 100 == 0):
                print(f"\nIteration {i}/{iterations}")
                print("--" * 12)
                print(f"Loss: {loss}, dW: {dW }, db: {db }")
                print(f"Coefficients: {self.__params['intercept'][0]} & {self.__params['coef'].T[0]}")
                
            if(prev_loss != None and prev_loss - loss < self.__convergence_threshold):
                print(f"converged after {i} iterations")
                break
            prev_loss = loss
            
        print(f"Final Coefficients: i: {self.__params['intercept'][0]} & w: {self.__params['coef'].T[0]}")

        
    def get_observation_counts(self, federatedHospitals):
        observed1 = 0
        observed2 = 0
        for hospital in federatedHospitals:
            observed1 += hospital.runQuery("count_observations_neg")
            observed2 += hospital.runQuery("count_observations_pos")
        
        return observed1, observed2
    
    def get_probs(self, federatedHospitals, learning_rate=0.05, iterations=1000):
        # logistic regression
        self.__fit(federatedHospitals,learning_rate, iterations) # weights should be distributed, now get predictions per hospital

        probs1 = pd.Series()
        probs2 = pd.Series()
        for hospital in federatedHospitals:
            probs1 = probs1.append(hospital.runQuery("predict_proba_neg", self.__params), ignore_index=True)
            probs2 = probs2.append(hospital.runQuery("predict_proba_pos", self.__params), ignore_index=True)
        return probs1, probs2
import numpy as np # linear algebra
import pandas as pd
from sklearn.preprocessing import StandardScaler
import sys
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt # data visualization

class MyLogisticRegBinary(BaseEstimator, ClassifierMixin):
    def __init__(self):
        self.params = {}
        self.verbose = 0
        self.__convergence_threshold = 0.0001

    def init_params(self, X):
        """Randomly initializing parameters"""
        n_features = X.shape[1]  # number of features
        # the matrix for slope coefficients
        self.params['coef'] = np.zeros((n_features, 1)) #np.random.randn(self.n_features, 1)
        self.params['intercept'] = np.ones((1, 1))   #np.random.randn(1, 1)
#         self.params['coef'] = np.random.randn(n_features, 1)
#         # the y-intercept
#         self.params['intercept'] = np.random.randn(1, 1)
        if self.verbose:
            print("[INFO] Initialized parameters.")
            print(f"Coefficients: {self.params['intercept'][0]} & {self.params['coef'].T[0]}")

    def get_logits(self, X, y=None):
        # logits = log(odds) = X@W + b
        if 'coef' not in self.params:
            # initialize the parameters if haven't
            self.init_params(X)
        return X @ self.params['coef'] + self.params['intercept']
    
    def predict_proba(self, X, y=None):
        """
        Sigmoid function, aka logistic function.
        Used to get probability for binary classification.
        """
        logits = self.get_logits(X)
        logits
        return 1 / (1 + np.exp(-logits))
    
    def gradients(self, X, y, y_hat):
        m = X.shape[0]
        dw = (1/m)*(X.T @ (y_hat - y))
        db = (1/m)*np.sum((y_hat - y)) 
        return dw, db

    def fit(self, X, y, learning_rate=0.05, iterations=1000):
        # set verbose to 1 to see the entire training progress
        
        # convert Dataframe to numpy for faster computations 
        if isinstance(X, pd.DataFrame):
            X = X.values
        if isinstance(y, pd.DataFrame):
            y = y.values
        # initialize parameters
        self.init_params(X)
        m = X.shape[0]  # number of samples
    
        if self.verbose:
            print("[INFO] Training ...")
        prev_loss = None
        for i in range(1, iterations + 1):
            # make predictions by computing probability
            y_proba = self.predict_proba(X)
            # calculate the binary cross-entropy loss
            loss = - (1 / m) * np.sum(y * np.log(y_proba) + (1 - y) * np.log(1 - y_proba))
            dW, db = self.gradients(X, y, y_proba)

            # use gradient descent to update parameters
            # parameter = parameter - (learning_rate * derivative_of_parameter
            self.params['coef'] -= (learning_rate * dW)
            self.params['intercept'] -= (learning_rate * db)
            if self.verbose: # and (i == 1 or i % 100 == 0):
                print(f"\nIteration {i}/{iterations}")
                print("--" * 12)
                print(f"Loss: {loss}, dW: {dW}, db: {db}")
                print(f"Coefficients: {self.params['intercept'][0]} & {self.params['coef'].T[0]}")
        
            if(prev_loss != None and prev_loss - loss < self.__convergence_threshold):
                print(f"converged after {i} iterations")
                break
            prev_loss = loss
                
        print(f"Final Coefficients: {self.params['intercept'][0]} & {self.params['coef'].T[0]}")

    def predict(self, X):
        return self.predict_proba(X)
    
    def predict_score(self, X, y, threshold=0.5):
        from sklearn.metrics import accuracy_score
        y_proba = self.predict(X)
        y_pred = np.where(y_proba > threshold, 1, 0)
        return accuracy_score(y, y_pred)


def plotLogReg(lr: LogisticRegression, X, dmin = -1, dmax = -1, N = 50, scale = 100): # scale because range uses ints and we want to increase number of points
    lr.intercept_
    lr.coef_
    # scaled min to max range:
    dmin = int(X.min()*scale) if (dmin == -1) else dmin * scale
    dmax = int(X.max()*scale) if (dmax == -1) else dmax * scale
    min_to_max = pd.Series(map(lambda x: x / scale, list(range(dmin,dmax,int((dmax-dmin)/N))))).to_frame()
    probs = lr.predict_proba(min_to_max)[:, 1]
    plt.clf()
    plt.scatter(np.array(min_to_max.T), probs)
    plt.show()
    
def plotMyLogReg(my_lr: MyLogisticRegBinary, X, dmin = -1, dmax = -1, N = 50, scale = 100):
    # scaled min to max range:
    dmin = int(X.min()*scale) if (dmin == -1) else dmin * scale
    dmax = int(X.max()*scale) if (dmax == -1) else dmax * scale
    min_to_max = pd.Series(map(lambda x: x / scale, list(range(dmin,dmax,int((dmax-dmin)/N))))).to_frame()
    my_probs = my_lr.predict_proba(min_to_max)
    plt.clf()
    plt.scatter(np.array(min_to_max.T), my_probs)
    plt.show()
    
def plotLogRegX(lr: LogisticRegression, X):
    lr.intercept_
    lr.coef_
    probs = lr.predict_proba(X)[:, 1]
    plt.clf()
    plt.scatter(np.array(X.T), probs)
    plt.show()
    
def plotMyLogRegX(my_lr: MyLogisticRegBinary, X):
    my_probs = my_lr.predict_proba(X)
    plt.clf()
    plt.scatter(np.array(X.T), my_probs)
    plt.show()

    
np.random.seed(42)
# from sklearn.base import BaseEstimator, ClassifierMixin
class FederatedError(Exception):
    """Raised when query is not secure"""
    pass
class SecurityError(FederatedError):
    """Raised when query is not secure"""
    pass
class NumberOfFeaturesError(FederatedError):
    """number of features is not the same on all federated samples"""
    pass
class InsufficientDataError(FederatedError):
    """Insufficient local data to remain anonymous"""
    pass
class InvalidQueryError(FederatedError):
    """Invalid query, does not exist"""
    pass
class NoMutationOrAllMutationError(FederatedError):
    """Mutation data contains no mutations or all samples are mutated"""
    pass
class InvalidQueryError(FederatedError):
    """Invalid query requested"""
    pass

class federatedSystemWithSecurityLayer: # simulation
    def __init__(self, localX, localy):
        if isinstance(localX, pd.DataFrame):
            localX = localX.values
        if isinstance(localy, pd.DataFrame):
            localy = localy.values
        self.__localX = localX
        self.__localy = localy
        self.__convergence_threshold = 0.0001
        self.__minimumDataForAnonymousData = 10
        
        self.__n_samples = self.__localX.shape[0]   # number of samples
        self.__n_features = self.__localX.shape[1]  # number of features
        self.__params = {} 
        self.__validQueries = ["deltas", "weights", "prep", "predict_proba", "get_n_features", "get_n_samples"]
    
    # private functions
    def __setParams(self, params): # all should start with the same values
        self.__params['coef'] = params['coef']
        self.__params['intercept'] = params['intercept']
    def __isQueryValid(self, query):
        if query not in self.__validQueries:
            return False
        else:
            return True
        
    def __getLocalDataCountForQuery(self, query = None):
        return len(self.__localX)
    
    def __getLogits(self):
        # logits = log(odds) = X@W + b
        return self.__localX @ self.__params['coef'] + self.__params['intercept']
    
    def __predictProbabilities(self):
        """
        Sigmoid function, aka logistic function.
        Used to get probability for binary classification.
        """
        logits = self.__getLogits()
        return 1 / (1 + np.exp(-logits))
    
    def __isSecure(self, query):
        if not self.__isQueryValid(query):
            raise InvalidQueryError
        if self.__getLocalDataCountForQuery(query) < self.__minimumDataForAnonymousData:
            raise InsufficientDataError
        if "raw" in query: # dummy to filter queries on raw data...
            raise SecurityError
    
    # queries
    def __queryDeltasForLogReg(self, data):
        self.__setParams(data)        
        y_proba = self.__predictProbabilities()
        loss = - np.sum(self.__localy * np.log(y_proba) + (1 - self.__localy) * np.log(1 - y_proba)) # / self.__n_samples
        # Gradient of loss w.r.t weights.
        dw = np.dot(self.__localX.T, (y_proba - self.__localy)) # / self.__n_samples
        # Gradient of loss w.r.t bias.
        db = np.sum((y_proba - self.__localy))  # / self.__n_samples
        
#         print(f"dW: {dw} and db {db}") 
        return loss, dw, db
    
    def __queryPrepLocalData(self, scale=True):
        if isinstance(self.__localX, pd.Series):  # for a single covariate, the user can provide data as a pd.Series
            self.__localX = self.__localX.to_frame()

        common_samples = np.intersect1d(self.__localy.index.values, self.__localX.index.values)
        self.__localy = self.__localy.loc[common_samples].values.flatten()
        if len(np.unique(y)) == 1:
            raise NoMutationOrAllMutationError #warnings.warn("Mutation data contains no mutations or all samples are mutated.")
            #return pd.Series(y.astype(float), index=common_samples)
        self.__localX = self.__localX.loc[common_samples].values
        if scale:
            ss = StandardScaler()
            self.__localX = ss.fit_transform(self.__localX)
            
    def __queryGetNumFeatures(self):
        return self.__n_features
    
    def __queryPredictProba(self):
        return __predictProbabilities()
         
    def __queryGetNSamples(self):
        return self.__getLocalDataCountForQuery()
              
    def __querySetParams(self, data):
        self.__setParams(data)
        
    # run query as a service
    def runQuery(self, query, data = None):
        try:
            self.__isSecure(query)
            # select and execute query:
            if query.startswith("deltas"):
                return self.__queryDeltasForLogReg(data)
            elif query.startswith("weights"):
                self.__querySetParams(data)
            elif query.startswith("prep"):
                self.__queryPrepLocalData()
            elif query.startswith("predict_proba"):
                return self.__queryPredictProba()
            elif query.startswith("get_n_features"):
                return self.__queryGetNumFeatures()
            elif query.startswith("get_n_samples"):
                return self.__queryGetNSamples()
            else:
                raise InvalidQueryError
        except FederatedError:
            raise            
        

class FederatedLogisticRegression:
    def __init__(self, federatedHospitals):
        self.params = {}
        self.verbose = 0
        # check if n_feautres is the same for all:
        self.n_features = federatedHospitals[0].runQuery("get_n_features")
        for hospital in federatedHospitals:
            if hospital.runQuery("get_n_features") != self.n_features:
                raise NumberOfFeaturesError
        self.params['coef'] = np.zeros((self.n_features, 1)) #np.random.randn(self.n_features, 1)
        self.params['intercept'] = np.ones((1, 1))   #np.random.randn(1, 1)
        if self.verbose:
            print(f"Coefficients: {self.params['intercept'][0]} & {self.params['coef'].T[0]}")
        
        self.__convergence_threshold = 0.0001
                
        for hospital in federatedHospitals:
               hospital.runQuery("weights", self.params)
         
    def getLogits(self, X):
        # logits = log(odds) = X@W + b
        return X @ self.params['coef'] + self.params['intercept']
    
    def predict_proba(self, X):
        """
        Sigmoid function, aka logistic function.
        Used to get probability for binary classification.
        """
        logits = self.getLogits(X)
        return 1 / (1 + np.exp(-logits))
    
               
    def fit(self, federatedHospitals, learning_rate=0.05, iterations=1000):
#         print(f"Initial Coefficients: {self.params['intercept'][0]} & {self.params['coef'].T[0]}")
        prev_loss = None
        n_samples = 0
        for hospital in federatedHospitals:
            n_samples += hospital.runQuery("get_n_samples")
        for i in range(1, iterations + 1):
            dW = 0
            db = 0
            loss = 0
            for hospital in federatedHospitals:
#                 hospital_local_n_samples, 
                hospital_local_loss, hospital_local_dW, hospital_local_db = hospital.runQuery("deltas", self.params)
#                 n_samples += hospital_local_n_samples
                loss += hospital_local_loss
                dW += hospital_local_dW
                db += hospital_local_db 
                
            loss = loss / n_samples
            dW = dW / n_samples
            db = db / n_samples
            self.params['coef'] -= (learning_rate * dW )
            self.params['intercept'] -= (learning_rate * db )
            
            if self.verbose: # and (i == 1 or i % 100 == 0):
                print(f"\nIteration {i}/{iterations}")
                print("--" * 12)
                print(f"Loss: {loss}, dW: {dW }, db: {db }")
                print(f"Coefficients: {self.params['intercept'][0]} & {self.params['coef'].T[0]}")
            if(prev_loss != None and prev_loss - loss < self.__convergence_threshold):
                print(f"converged after {i} iterations")
                break
            prev_loss = loss
               
        print(f"Final Coefficients: {self.params['intercept'][0]} & {self.params['coef'].T[0]}")
        # distiribute final weights
        for hospital in federatedHospitals:
            hospital.runQuery("weights", self.params)
            
    def predict(self, X, threshold=0.5):
        y_proba = self.predict_proba(X)
        y_pred = np.where(y_proba > threshold, 1, 0)
        return y_pred
    
    def predictScore(self, X, y):
        from sklearn.metrics import accuracy_score
        y_pred = self.predict(X)
        return accuracy_score(y, y_pred)
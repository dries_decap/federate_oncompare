import pandas as pd
import numpy as np # linear algebra
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
import federated_model

def getData(path):
    df = pd.read_csv(path)
    pos_group = df.loc[df.LNI == 1]
    neg_group = df.loc[df.LNI == 0]
    mutation_status_negatives = neg_group["TP53"]
    mutation_status_positives = pos_group["TP53"]
    covariates_negatives = neg_group[["TMB"]] #, "RND", "AGE_RND"]]
    covariates_positives = pos_group[["TMB"]] #, "RND", "AGE_RND"]] # TMB, AGE_RND, RND
    return mutation_status_negatives, mutation_status_positives, covariates_negatives, covariates_positives

# prep data
def prepLocalData(mut_present_series, covariates_DF, common_samples, scale):
    if isinstance(covariates_DF, pd.Series):  # for a single covariate, the user can provide data as a pd.Series
        covariates_DF = covariates_DF.to_frame()
    if isinstance(mut_present_series, pd.Series):  # for a single covariate, the user can provide data as a pd.Series
        mut_present_series = mut_present_series.to_frame()
        
    y = mut_present_series.loc[common_samples].values #.flatten()
    if len(np.unique(y)) == 1:
        warnings.warn("Mutation data contains no mutations or all samples are mutated.")
        if return_model:
            IOError("No model could be returned as the provided gene is mutated in all or none of the genes.")
        else:
            return pd.Series(y.astype(float), index=common_samples)
    X = covariates_DF.loc[common_samples].values
    if scale:
        ss = StandardScaler()
        X = ss.fit_transform(X)
    return y, X

def poisson_binom_pmf(probs):
    pmf = np.zeros(len(probs) + 1)

    pmf[0] = 1. - probs[0]
    pmf[1] = probs[0]

    for prob in probs[1:]:
        pmf = ((1-prob) * pmf + np.roll(pmf, 1) * prob)

    return pmf

def my_log_reg_model(X, y, common_samples):
    # logistic regression
    lr = federated_model.MyLogisticRegBinary();
    lr.fit(X, y)
    return pd.Series(lr.predict_proba(X)[:, 0], index=common_samples)

def log_reg_model(X, y, common_samples):
    # logistic regression
    lr = LogisticRegression(
            penalty='none',
            max_iter=200,  # 
            warm_start=True)  # prevent refreshing weights when fitting #C=C) #, **lr_kwargs)    
    lr.fit(X, y)
    print(f"model weights: {lr.coef_}, {lr.intercept_}")
    return pd.Series(lr.predict_proba(X)[:, 1], index=common_samples)

def prep_data(mut_data1, mut_data2, covariates1, covariates2, scale):
    # perform 2  datasets test
    if isinstance(mut_data1, pd.Series):
        mut_data1 = mut_data1.to_frame()
    if isinstance(mut_data2, pd.Series):
        mut_data2 = mut_data2.to_frame()
    if isinstance(covariates1, pd.Series):
        covariates1 = covariates1.to_frame()
    if isinstance(covariates2, pd.Series):
        covariates2 = covariates2.to_frame()


    common_genes = np.intersect1d(mut_data1.columns.values, mut_data2.columns.values)
    assert len(common_genes) > 0, "Please assure that the mut series have matching column labels"
    mut_data1, mut_data2 = mut_data1[common_genes], mut_data2[common_genes]

    # remove NaNs in case there are present:
    covariates2 = covariates2.dropna(axis=0)
    covariates1 = covariates1.dropna(axis=0)

    # find common samples between mut_data1 and clin_data1
    common_samples1 = np.intersect1d(mut_data1.index.values, covariates1.index.values)
    assert len(common_samples1) > 0, "The sample ids are not matching for the first dataset." \
                                     "Please assure that both the clinical data and the covariates" \
                                     " have the sample ids as their index."
    mut_data1, covariates1 = mut_data1.loc[common_samples1], covariates1.loc[common_samples1]
    # same for data2
    common_samples2 = np.intersect1d(mut_data2.index.values, covariates2.index.values)
    assert len(common_samples2) > 0, "The sample ids are not matching for the first dataset." \
                                     "Please assure that both the clinical data and the covariates" \
                                     " have the sample ids as their index."

    mut_data2, covariates2 = mut_data2.loc[common_samples2], covariates2.loc[common_samples2]

    assert len(np.intersect1d(covariates1.columns.values, covariates2.columns.values)) > 0, "Please make sure that the" \
                                                                     " columns of the covariates have identical names."

    mut_data = pd.concat([mut_data1, mut_data2], axis=0)
    covariates = pd.concat([covariates1, covariates2], axis=0)

    # prep data
    common_samples = np.intersect1d(mut_data.index.values, covariates.index.values)
    y, X = prepLocalData(mut_data, covariates, common_samples, scale)
    
    return X, y, common_samples, common_samples1, common_samples2

def get_probs(mut_data1, mut_data2, covariates1, covariates2, scale, mymodel = False):
    X, y, common_samples, common_samples1, common_samples2 = prep_data(mut_data1, mut_data2, covariates1, covariates2, scale)
    
    if mymodel:
        probs = my_log_reg_model(X, y, common_samples)
    else:
        probs = log_reg_model(X, y, common_samples)

    if isinstance(probs, dict):  # this is only when mut_data contains more than 1 gene
        probs = pd.DataFrame(probs)
        probs = 1. - (1. - probs).prod(axis=1)

    probs1 = probs.loc[common_samples1]
    probs2 = probs.loc[common_samples2]
    return probs1, probs2


def getPVal(observed1, observed2, probs1, probs2):

#     print(f"observed: {observed1} and {observed2}")
#     print(f"len probs: {len(probs1)} and {len(probs2)}")
#     print(probs2)
    
    large_number = 1e12  # needs to be larger than a typical dataset
    tail='two-sided' # default value
    # ratio
    if (observed1 < 1) and (observed2 < 1):
        if tail != "two-sided":
            pval = 0.5
        else:
            pval = 1.
    else:
        if observed2 < 1:
            observed_ratio = observed1 * large_number
        elif observed1 < 1:
            observed_ratio = 1. / (observed2 * large_number)
        else:
            observed_ratio = 1. * observed1 / observed2

        nsamples1, nsamples2 = len(probs1), len(probs2)
        set1_range, set2_range = np.arange(nsamples1 + 1, dtype=float), np.arange(nsamples2 + 1, dtype=float)
        set2_range[0], set1_range[0] = 1./large_number, 1./large_number

        prob_mask = set1_range[..., None] / set2_range[None, ...]
        # prob_mask[0, :] = 0.
        prob_mask[0, 0] = 1.

        pmf_set1 = poisson_binom_pmf(probs1.values) # was without .values but gives an error then...
        pmf_set2 = poisson_binom_pmf(probs2.values) # was without .values but gives an error then...

        joint_pmf = np.outer(pmf_set1, pmf_set2)

        if tail == 'left':
            pval = joint_pmf[prob_mask <= observed_ratio].sum()

        elif tail == 'right':
            pval = joint_pmf[prob_mask >= observed_ratio].sum()

        else:
            pval_left = joint_pmf[prob_mask <= observed_ratio].sum()
            pval_right = joint_pmf[prob_mask >= observed_ratio].sum()
            pval = np.minimum(2 * np.minimum(pval_left, pval_right), 1.)

    print(f"pval is {pval}")
    return pval
#!/usr/bin/env bash
set -eux

VERSION=1.0.0
TAG=$VERSION

docker build --pull --rm -f "Dockerfile" -t feder8/distributed-analytics-example:$TAG "."

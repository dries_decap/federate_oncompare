#!/usr/bin/env python
import sys
import os
import psycopg2
import logging
import utils

from pyfeder8.config.ConfigurationClient import ConfigurationClient


if __name__ == "__main__":

    log_level = os.environ.get('LOG_LEVEL', 'INFO').upper()
    logging.basicConfig(level=log_level)

    try:
        therapeutic_area = os.environ.get("THERAPEUTIC_AREA", "honeur")
        config_name = "feder8-config-" + therapeutic_area.lower()
        print(config_name)
        configuration = ConfigurationClient(config_server="http://config-server:8080/config-server", config_name=config_name).get_configuration()
        print(configuration)
        db_connection_details = configuration.db_connection_details
        print(db_connection_details)
        vocabulary_schema = db_connection_details.vocab_schema
        cdm_schema = db_connection_details.cdm_schema
        #db_connect_str = configuration.db_connection_details.connect_string(True)
        #engine = DatabaseConnection.get_db_engine(configuration.db_connection_details)
    except Exception as e:
        logging.exception("The local configuration could not be loaded!")
        sys.exit("The local configuration could not be loaded!")

    output_file = os.environ.get('OUTPUT_FILE')

    if output_file is None:
        sys.exit("Output file location should be defined as environment variable OUTPUT_FILE")

    query = f"select count(person_id) from {cdm_schema}.person"
    output_query = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

    with psycopg2.connect(host=db_connection_details.host,
                          port=db_connection_details.port,
                          dbname=db_connection_details.name,
                          user=db_connection_details.username,
                          password=db_connection_details.password,
                          options="-c search_path=" + cdm_schema) as connection:
        connection.autocommit = True

        with connection.cursor() as cursor:
            logging.info(f"Start running SQL Query '{query}' and exporting to file")
            with open(output_file, 'w') as f:
                cursor.copy_expert(output_query, f)
            logging.info("Done running SQL Query")

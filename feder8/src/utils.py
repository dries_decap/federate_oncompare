import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
import json


# datafields to use in Logistic Regression
data_fields = ["TMB"] #, "RND", "TP53"]
mutation_status_fields = ["TP53"]

# FEDER8 FUNCTIONS
def get_sum_feder8(outputfile):
    mutation_status, covariates, pos_group, neg_group = load_dataset("$HOME/data/local_data.csv")
    sum = get_sum(covariates)
    count = len(covariates)
    data = {'sum': sum, 'count': count}
    json_object = json.dumps(data)
    # write to outputfile:
    with open(outputfile, "w") as outfile:
        outfile.write(json_object)
    print(f"json_object: {json_object}")
    return json_object

def get_sum_of_squares_feder8(outputfile, mean):
    mutation_status, covariates, pos_group, neg_group = load_dataset("$HOME/data/local_data.csv")
    sumofsquares = get_sum_of_squares(covariates, mean)
    count = len(covariates)
    data = {'sumofsquares': sumofsquares, 'count': count}
    json_object = json.dumps(data)
    # write to outputfile:
    with open(outputfile, "w") as outfile:
        outfile.write(json_object)
    print(f"json_object: {json_object}")
    return json_object

def fit_and_get_new_weights(outputfile, mean, std_dev, weights):
    mutation_status, covariates, pos_group, neg_group = load_dataset("$HOME/data/local_data.csv")
    covariates = scale_covariates(covariates, mean, std_dev):
    model = load_new_model()
    fit(model, covariates, mutation_status, weights)
    loss = evaluate(model, covariates, mutation_status, weights)

    data = {'d_weights': d_weights, 'loss': loss}
    json_object = json.dumps(data)
    # write to outputfile:
    with open(outputfile, "w") as outfile:
        outfile.write(json_object)
    print(f"json_object: {json_object}")
    return json_object

def


# LOG REG MANAGEMENT
def get_model_weights(model: LogisticRegression):
    """Returns the paramters of a sklearn LogisticRegression model."""
    if model.fit_intercept:
        params = [model.coef_, model.intercept_]
    else:
        params = [model.coef_,]
    return params


def set_model_weights(model: LogisticRegression, weights) -> LogisticRegression:
    """Sets the parameters of a sklean LogisticRegression model."""
    model.coef_ = weights[0]
    if model.fit_intercept:
        model.intercept_ = weights[1]
    return model


def set_initial_weights(model: LogisticRegression):
    """Sets initial parameters as zeros Required since model params are
    uninitialized until model.fit is called.
    But server asks for initial parameters from clients at launch. Refer
    to sklearn.linear_model.LogisticRegression documentation for more
    information.
    """
    n_classes = 1  # oncompare dataset has only 1 class
    n_features = len(data_fields)  # Number of features in dataset
    model.classes_ = np.array([i for i in range(n_classes)])

    model.coef_ = np.zeros((n_classes, n_features))
    if model.fit_intercept:
        model.intercept_ = np.zeros((n_classes,))

def load_new_model():
    # Create LogisticRegression Model
    model = LogisticRegression(
        penalty='none',
        max_iter=1,  # local epoch
        warm_start=True,  # prevent refreshing weights when fitting
    )
    # Setting initial parameters, akin to model.compile for keras models
    utils.set_initial_weights(model)
    return model

def fit(model, X, y, weights):
    model = set_model_weights(model, weights)
    print(f"weights used: {weights}")
    # Ignore convergence failure due to low local epochs
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        model.fit(X, y)
    # log(DEBUG, "Training finished for round %s", config['rnd'])
    return utils.get_model_weights(model)

def evaluate(model, X, y, weights):  # type: ignore
    model = set_model_weights(model, weights)
    loss = log_loss(y, model.predict_proba(X))
    accuracy = model.score(X, y)
    print(f"Loss is {loss} and accuracy is {accuracy}")
    return loss

def get_probabilities(model, X, group):
    return model.predict_proba(X.loc[group])[:, 1]


# DATA MANAGEMENT
def prep_data(mutation_status, covariates):
    # check if all fields are available:
    assert len(mutation_status.columns.values) == len(mutation_status_fields), "mutation data 1 does not have all requested mutation status fields"
    assert len(covariates.columns.values) == len(data_fields), "covariates 1 does not have all requested data fields"

    # remove NaNs in case there are present:
    covariates = covariates.dropna(axis=0)

    # only keep samples where mutation status fields and data fields are all present
    common_samples = np.intersect1d(mutation_status.index.values, covariates.index.values)
    assert len(common_samples) > 0, "The sample ids are not matching for the first dataset." \
                                     "Please assure that both the clinical data and the covariates" \
                                     " have the sample ids as their index."

    # sanity check, not needed in multiple hospital setting?
    # assert len(np.unique(mutation_status.loc[common_samples])) > 1, "Mutation data contains no mutations or all samples are mutated."

    # returns prepped unscaled data
    return mutation_status.loc[common_samples].values, covariates.loc[common_samples].values

def load_dataset(path): # -> Dataset:
    """Loads dataset from path
    """
    df = pd.read_csv(path)
    pos_group = df.loc[df.LNI == 1].index.values
    neg_group = df.loc[df.LNI == 0].index.values
    mutation_status, covariates = prep_data(df[mutation_status_fields], df[data_fields])
    # returns: y, X (unscaled), pos_group indexes, neg_group indexes
    return mutation_status, covariates, pos_group, neg_group

def scale_covariates(covariates, mean, std_dev):
    return ( covariates * mean ) / std_dev

def get_probabilities(model, X, pos_group, neg_group):
    return model.predict_proba(X.loc[pos_group])[:, 1], model.predict_proba(X.loc[neg_group])[:, 1]

def get_observed_count(mutation_status, pos_group, neg_group):
    return np.sum(mutation_status.loc[pos_group].values > 0).item(), np.sum(mutation_status.loc[neg_group].values > 0).item()

def get_sum(X):
    return X.sum(axis=0).values.flatten().tolist()

def get_sum_of_squares(X, mean):
    return np.sum(np.power(X - mean, 2)).values.flatten().tolist()

# OnCompare functions
def poisson_binom_pmf(probs):
    pmf = np.zeros(len(probs) + 1)

    pmf[0] = 1. - probs[0]
    pmf[1] = probs[0]

    for prob in probs[1:]:
        pmf = ((1-prob) * pmf + np.roll(pmf, 1) * prob)

    return pmf

def get_pval(observed1, observed2, probs1, probs2):
    #     print(f"observed: {observed1} and {observed2}")
    #     print(f"len probs: {len(probs1)} and {len(probs2)}")
    #     print(probs2)

    large_number = 1e12  # needs to be larger than a typical dataset
    tail='two-sided' # default value
    # ratio
    if (observed1 < 1) and (observed2 < 1):
        if tail != "two-sided":
            pval = 0.5
        else:
            pval = 1.
    else:
        if observed2 < 1:
            observed_ratio = observed1 * large_number
        elif observed1 < 1:
            observed_ratio = 1. / (observed2 * large_number)
        else:
            observed_ratio = 1. * observed1 / observed2

        nsamples1, nsamples2 = len(probs1), len(probs2)
        set1_range, set2_range = np.arange(nsamples1 + 1, dtype=float), np.arange(nsamples2 + 1, dtype=float)
        set2_range[0], set1_range[0] = 1./large_number, 1./large_number

        prob_mask = set1_range[..., None] / set2_range[None, ...]
        # prob_mask[0, :] = 0.
        prob_mask[0, 0] = 1.

        pmf_set1 = poisson_binom_pmf(probs1) # was without .values but gives an error then...
        pmf_set2 = poisson_binom_pmf(probs2) # was without .values but gives an error then...

        joint_pmf = np.outer(pmf_set1, pmf_set2)

        if tail == 'left':
            pval = joint_pmf[prob_mask <= observed_ratio].sum()

        elif tail == 'right':
            pval = joint_pmf[prob_mask >= observed_ratio].sum()

        else:
            pval_left = joint_pmf[prob_mask <= observed_ratio].sum()
            pval_right = joint_pmf[prob_mask >= observed_ratio].sum()
            pval = np.minimum(2 * np.minimum(pval_left, pval_right), 1.)

    # print(f"pval is {pval}")
    return pval

#!/usr/bin/env bash

if [ "$1" = 'run-script' ]; then
  set -e
  exec python3 -u main.py
fi

exec "$@"
#!/usr/bin/env bash
set -eux

VERSION=1.0.0
TAG=$VERSION
THERAPEUTIC_AREA_URL=harbor-uat.athenafederation.org

docker tag feder8/distributed-analytics-example:$TAG $THERAPEUTIC_AREA_URL/distributed-analytics/distributed-analytics-example:$TAG
docker push $THERAPEUTIC_AREA_URL/distributed-analytics/distributed-analytics-example:$TAG

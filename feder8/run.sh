#!/usr/bin/env bash

VERSION=1.0.0
TAG=$VERSION

OUTPUT_FILE_HOST=${PWD}/output.csv
OUTPUT_FILE=/opt/data/output.csv

touch da-example.env
echo "OUTPUT_FILE=${OUTPUT_FILE}" >> da-example.env
echo "THERAPEUTIC_AREA=HONEUR" >> da-example.env

touch $OUTPUT_FILE_HOST

docker run \
--rm \
--name distributed-analytics-example \
-v $OUTPUT_FILE_HOST:$OUTPUT_FILE \
--env-file da-example.env \
--network feder8-net \
feder8/distributed-analytics-example:$TAG

rm -rf da-example.env
